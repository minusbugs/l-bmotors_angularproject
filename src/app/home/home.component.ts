import { Component, OnInit } from '@angular/core';
import {CarService} from '../shared/car.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private carService:CarService) { }
  profile ={};
  cars:any[]=[];
  

  loadUser(){
    this.carService.getUser().subscribe(data=>this.profile=data);
  }

  loadRecentCar(){
    this.carService.getRecentCars().subscribe(data=>this.cars=data);
  }

  ngOnInit() {
    this.loadRecentCar();
  }

}
