import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { CarsComponent } from './cars/cars.component';
import { ContactsComponent } from './contacts/contacts.component';
import { AppRoutingModule } from './/app-routing.module';

import {CarService} from '../app/shared/car.service';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutusComponent,
    CarsComponent,
    ContactsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpModule 
  ],
  providers: [CarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
